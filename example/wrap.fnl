(local {:start start-repl} (require :lib.stdio))

(local {: newImage : newMesh : draw} love.graphics)
(local {: generate-points} (require :lib.sqcr))

(local points (generate-points 64 64 10))
(local icon (newImage :icon-64.png))
(local mesh (newMesh points :fan))
(mesh:setTexture icon)

(fn love.load []
  (start-repl))

(fn love.draw []
  (draw mesh 100 100))
