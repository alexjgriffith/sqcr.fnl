VERSION=0.1.0
NAME=sqcr
URL=https://gitlab.com/alexjgriffith/love-libs
AUTHOR="Alexander Griffith"
DESCRIPTION="Create 2D point arrays for rectangles with rounded corners"

SRC := $(NAME).fnl
OUT := $(patsubst %.fnl,%.lua,$(SRC))

%.lua: %.fnl; lua example/lib/fennel --compile --correlate $< > $@

build: $(OUT); mv $(NAME).lua init.lua

clean: ; rm -rf *.lua
