(local sqcr
       {:_LICENCE "MIT"
        :_VERSION "0.1.0"
        :_AUTHOR "Alexander Griffith"
        :_URL "https://gitlab.com/alexjgriffith/sqcr.fnl"
        :_DESCRIPTION: "Round the corners of images"})

(fn add-points [function mid r resolution ?step]
  (let [step (or ?step 0.15)]
    (match resolution
      0 [(function (math.cos (* math.pi mid))
                   (math.sin (* math.pi mid))
                   r)]
      _ (let [a (add-points function (+ mid step) r (- resolution 1) (/ step 2))
              b (function (math.cos (* math.pi mid))
                          (math.sin (* math.pi mid))
                          r)
              c (add-points function (- mid step) r (- resolution 1) (/ step 2))]
          (table.insert a b)
          (each [key value (ipairs c)]
            (table.insert a value))
          a))))

(fn arc-segments [s e w h r index]
  (local resolution (math.min (math.floor (/ (* w h) (^ 2 6))) 4))
  ;; add-points is not tail recursive, try to keep resoltion low
  ;; be default the cut off is 4, 1 -> 3, 2-> 7, 3-> 15, 4-> 31
  ;; per corner
  (local ret [s])
  (local m
         (match index
           :1  (let [add-point (fn [c s r]
                                 [(- (- w r) (* 1 r c))
                                  (+ r  (* r s))])]                 
                 (add-points add-point 1.25 r resolution))
           :2 (let [add-point (fn [c s r]
                                [(- (- w r) (* 1 r c))
                                 (+ (- h r)  (* r s))])]
                (add-points add-point 0.75 r resolution))
           :3 (let [add-point (fn [c s r]
                                [(- r (* 1 r c))
                                 (+ (- h r)  (* r s))])]
                (add-points add-point 0.25 r resolution)) 
           :4 (let [add-point (fn [c s r]
                                [(- r (* 1 r c))
                                 (+ r  (* r s))])]
                (add-points add-point -0.25 r resolution))))
  (each [k v (ipairs m)]
    (table.insert ret v))
  (table.insert ret e)  
  ret)

(fn set-uv [array w h vl vt vw vh]
    (var flat [])
    (each [key members (ipairs array)]
      (for [i 1 (# members)]        
        (tset (. members i) 1 (+ (. members i 1) vl))
        (tset (. members i) 2 (+ (. members i 2) vt))
        (let [to-add (. members i)]
          (table.insert to-add (* (/ (. to-add 1) w) vw))
          (table.insert to-add (* (/ (. to-add 2) h) vh))
          (table.insert flat to-add))))
    flat)

(fn rounded-mesh [w h r ...]
  (let [array [(arc-segments [(- w r) 0] [w r] w h r :1)
               (arc-segments  [w (- h r)] [(- w r) h] w h r :2)
               (arc-segments  [r h] [0 (- h r)] w h r :3)
               (arc-segments  [0 r] [r 0] w h r :4)]] 
    (set-uv array w h ...)))

(fn rectangle [w h ...]
  (set-uv [[[0 0]
            [(+ 0 w) 0]
            [(+ 0 w) (+ 0 h)]
            [ 0 (+ 0 h)]]] w h ...))

(fn sqcr.generate-points [w h ?r vl? vt? vw? vh? iw? ih?]
  (assert w "Generate Points requires width (w)")
  (assert h "Generate Points requires height (h)")
  (let [iw (or iw? w)
        ih (or ih? h)
        vl (/ (or vl? 0) iw)
        vt (/ (or vt? 0) ih)
        vw (/ (or vw? w) iw)
        vh (/ (or vh? h) ih)
        r (or ?r 0)]
    (values (match ?r
              nil (rectangle w h)
              _ (rounded-mesh w h ?r vl vt vw vh))
            { : iw : ih : vl : vt : vw : vh : r})))

sqcr
